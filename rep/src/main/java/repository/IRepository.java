package repository;

import java.sql.SQLException;
import java.util.List;

import hibernate.EnumerationValue;

public interface IRepository<T> {
	public List<T> getAll();	
	public T getById(int id);
	public void save(T entity);
	public void delete(int id);

	List<T> read(String query, Object[] params);

	void setUpInsertQuery(T entity) throws SQLException;

	void setUpUpdateQuery(EnumerationValue entity) throws SQLException;

	String getTableName();

	String getInsertQuery();
	String getUpdateQuery();
	
	
}
