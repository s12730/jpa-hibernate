package repository.impl;

import java.sql.Connection;
import java.util.List;

import hibernate.Person;
import hibernate.UserRoles;
import repository.IRepositoryCatalog;
import repository.IUserRepository;
import repository.IEnumerationValueRepository;
import repository.IRepository;
import uow.IUnitOfWork;



public class HsqlRepositoryCatalog implements IRepositoryCatalog {
	private Connection connection;
	private IUnitOfWork uow;
	
	public HsqlRepositoryCatalog(Connection connection, IUnitOfWork uow) {
		this.connection = connection;
		this.uow = uow;
	}

	
	public void saveChanges(){
		uow.saveChanges();
	}

	public IUserRepository users() {
		return new HsqlUserRepository(connection, new UserBuilder(), uow);
	}

	public IEnumerationValueRepository enumerations() {
		return null;
	}


	public <TEntity> List<TEntity> read(String query, Object[] params, List<TEntity> HibernateRepository) {
		
		return null;
	}


	public IUserRepository getUsers() {
		// TODO Auto-generated method stub
		return null;
	}


	public IRepository<Person> getPersons() {
		// TODO Auto-generated method stub
		return null;
	}


	public IRepository<UserRoles> getRoles() {
		// TODO Auto-generated method stub
		return null;
	}


	public void commit() {
		// TODO Auto-generated method stub
		
	}
}