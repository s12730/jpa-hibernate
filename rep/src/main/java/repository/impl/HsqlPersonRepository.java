package repository.impl;

import org.hibernate.Session;

import hibernate.Person;
import java.sql.Connection;


import java.sql.SQLException;

import org.hibernate.sql.Insert;
import org.hibernate.sql.Update;


import uow.IUnitOfWork;

public class HsqlPersonRepository extends IRepository<Person>{

	public HsqlPersonRepository(Session s) {
		super(s, Person.class);
	}	
}
