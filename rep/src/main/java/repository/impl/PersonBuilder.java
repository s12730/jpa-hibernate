package repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import hibernate.Person;
import repository.IEntityBuilder;
public class PersonBuilder implements IEntityBuilder<Person> {

	public Person build(ResultSet rs) throws SQLException {
		Person person = new Person();
		person.setFirstName(rs.getString("name"));
		person.setSurname(rs.getString("surname"));
		person.setPesel(rs.getString("pesel"));
		person.setEmail(rs.getString("email"));
		person.setNip(rs.getString("nip"));
		person.setDateOfBirth(rs.getDate("dateOfBirth"));
		person.setId(rs.getInt("id"));		return person;
	}

	public <TEntity> List<TEntity> read(String query, Object[] params, List<TEntity> HibernateRepository) {
		return HibernateRepository;
		
		
	}

}
