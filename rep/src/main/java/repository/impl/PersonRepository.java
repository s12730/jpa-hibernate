package repository.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;

import hibernate.EnumerationValue;
import hibernate.Person;
import repository.IRepository;

public class PersonRepository extends BaseRepository<Person> implements IRepository<Person> {

	public PersonRepository(Session s) {
		super(s, Person.class);
	}

	public List<Person> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Person getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(Person entity) {
		// TODO Auto-generated method stub
		
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	public List<Person> read(String query, Object[] params) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setUpInsertQuery(Person entity) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public void setUpUpdateQuery(EnumerationValue entity) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getInsertQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getUpdateQuery() {
		// TODO Auto-generated method stub
		return null;
	}	
}
