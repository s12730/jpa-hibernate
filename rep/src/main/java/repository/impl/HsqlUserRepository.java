package repository.impl;
import org.hibernate.Session;

import hibernate.User;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.sql.Update;

import hibernate.EnumerationValue;
import hibernate.User;
import hibernate.UserRoles;
import repository.IEntityBuilder;
import repository.IUserRepository;
import repository.PagingInfo;
import uow.IUnitOfWork;

public class HsqlUserRepository extends Repository<User> implements IUserRepository
{
	
	public HsqlUserRepository(Connection connection, IEntityBuilder<User> builder, IUnitOfWork uow) {
		super(connection,builder, uow);
	}

	public HsqlUserRepository(Session s) {
		super(s, User.class);
	}

	public User withId(int id) {
		
		return null;
	}

	public List<User> allOnPage(PagingInfo page) {
		
		return null;
	}

	public void add(User entity) {
		
		
	}

	public void delete(User entity) {
		
		
	}

	public void modify(User entity) {

		
	}

	public int count() {
		
		return 0;
	}

	public List<User> read(String query, Object[] params) {
		
		return null;
	}

	public void setUpInsertQuery(User entity) throws SQLException {
		
		
	}

	public void setUpUpdateQuery(EnumerationValue entity) throws SQLException {
		
		
	}

	public String getTableName() {
		
		return null;
	}

	public String getInsertQuery() {
		
	}

	public String getUpdateQuery() {
		
		return null;
	}

	public User withLogin(String login) {
	
		return null;
	}

	public User withLoginAndPassword(String login, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setupPermissions(User user) {
		// TODO Auto-generated method stub
		
	}

	public List<User> withRole(UserRoles role) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<User> withRole(String roleName) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<User> withRole(int roleId) {
		// TODO Auto-generated method stub
		return null;
	}

	public <TEntity> List<TEntity> read(String query, Object[] params, List<TEntity> HibernateRepository) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setUpUpdateQuery(User entity) throws SQLException {
		// TODO Auto-generated method stub
		
	}
}
