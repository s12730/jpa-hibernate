package repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import hibernate.EntityClass;
import repository.IEntityBuilder;
import repository.IRepository;
import repository.PagingInfo;
import uow.IUnitOfWork;
import uow.IUnitOfWorkRepository;

@SuppressWarnings({ "unused", "rawtypes" })
public abstract class HibernateRepository<TEntity extends EntityClass, Repository> implements IRepository<TEntity>, IUnitOfWorkRepository {

	protected IUnitOfWork uow;
	protected Connection connection;
	protected PreparedStatement selectByID;
	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;
	protected PreparedStatement selectAll;
	protected IEntityBuilder<TEntity> builder;
	
	
	
	 
	private Repository repositoryimpl;

	 @SuppressWarnings({ "hiding", "unchecked" })
	public <TEntity> List<TEntity> read(String query, Object[] params, List<TEntity> HibernateRepository) {
		return  (List<TEntity>) read(query, params);
	    
	  }

	protected String selectByIDSql=
			"SELECT * FROM "
			+ getTableName()
			+ " WHERE id=?";
	protected String deleteSql=
			"DELETE FROM "
			+ getTableName()
			+ " WHERE id=?";
	protected String selectAllSql=
			"SELECT * FROM " + getTableName();
	

	protected HibernateRepository(Connection connection,IEntityBuilder<TEntity> builder, IUnitOfWork uow)
	{
		this.uow=uow;
		this.builder=builder;
		this.connection = connection;
		try {
			selectByID=connection.prepareStatement(selectByIDSql);
			insert = connection.prepareStatement(getInsertQuery());
			delete = connection.prepareStatement(deleteSql);
			update = connection.prepareStatement(getUpdateQuery());
			selectAll = connection.prepareStatement(selectAllSql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	public void add(TEntity entity) {
		uow.markAsNew(entity, this);
	}

	public void modify(TEntity entity) {
		uow.markAsChanged(entity, this);
	}

	public void delete(TEntity entity) {
		uow.markAsDeleted(entity, this);
	}

	public TEntity withId(int id) {
		try {
			selectByID.setInt(1, id);
			ResultSet rs = selectByID.executeQuery();
			while (rs.next()) {
				return builder.build(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public List<TEntity> allOnPage(PagingInfo page) {
		List<TEntity> result = new ArrayList<TEntity>();

		try {
			ResultSet rs = selectAll.executeQuery();
			while (rs.next()) {
				result.add(builder.build(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}

	
	public void persistAdd(EntityClass entity) {
		try {
			setUpInsertQuery((TEntity) entity);
			insert.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void persistUpdate(EntityClass entity) {

		try {
			setUpInsertQuery((TEntity) entity);
			update.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void persistDelete(EntityClass entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected abstract void setUpUpdateQuery(TEntity entity) throws SQLException;
	protected abstract void setUpInsertQuery(TEntity entity) throws SQLException;
	protected abstract String getTableName();
	protected abstract String getUpdateQuery();
	protected abstract String getInsertQuery();
	
}