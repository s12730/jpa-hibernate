package repository.impl;


	import java.util.List;

	import org.hibernate.Criteria;
	import org.hibernate.Session;
	import org.hibernate.criterion.Restrictions;

import repository.IRepository;

	public class BaseRepository<T> implements IRepository<T>{

		private Session session;
		private Class<T> entityClass;
		
		public BaseRepository(Session session, Class<T> entityClass){
			this.session = session;
			this.entityClass = entityClass;
		}
		
		@Override
		public List<T> getAll() {
			List<T> entity = createCriteria().list();
			return entity;
		}
		
		@Override
		public T getById(int id) {
			Criteria c = createCriteria();
			c.add(Restrictions.eq("id", id));
			return (T) c.list().get(0);
			
		}
		private Criteria createCriteria(){
			return session.createCriteria(entityClass);
		}

		@Override
		public void save(T entity) {
			session.saveOrUpdate(entity);		
		}

		@Override
		public void delete(int id) {
			T entityToDelete = getById(id);
			session.delete(entityToDelete);		
		}
		
		
	
}
