package repository.impl;
import repository.IEntityBuilder;
import repository.IEnumerationValueRepository;
import repository.PagingInfo;
import repository.impl.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.sql.Insert;
import org.hibernate.sql.Update;

import hibernate.EntityClass;
import hibernate.EnumerationValue;
import uow.IUnitOfWork;
import java.util.List;
import uow.*;
@SuppressWarnings("unused")
public class HsqlEnumerationValuesRepository extends Repository<EnumerationValue> implements IEnumerationValueRepository {

	public HsqlEnumerationValuesRepository(Connection connection, IEntityBuilder<EnumerationValue> builder, IUnitOfWork uow) {
		super(connection,builder,uow);
	}

	protected void setUpUpdateQuery(EnumerationValue entity) throws SQLException {
		Update.setInt(1, entity.getIntKey());
		Update.setString(2, entity.getStringKey());
		Update.setString(3, entity.getValue());
		Update.setString(4, entity.getEnumerationName());
	}


	protected void setUpInsertQuery(EnumerationValue entity) throws SQLException {
		Insert.setInt(1, entity.getIntKey());
		Insert.setString(2, entity.getStringKey());
		Insert.setString(3, entity.getValue());
		Insert.setString(4, entity.getEnumerationName());
	}

	protected String getTableName() {
		return "enumerations";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE enumerations SET (intKey, stringKey, value, enumerationName)=(?,?,?,?) WHERE id=?;";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO enumerations (intKey, stringKey, value, enumerationName) VALUES (?,?,?,?);";
	}


	public int count() {
		// TODO Auto-generated method stub
		return 0;
	}

	public EnumerationValue withName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public EnumerationValue withIntKey(int key, String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public EnumerationValue withStringKey(String key, String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<EnumerationValue> read(String query, Object[] params) {
		// TODO Auto-generated method stub
		return null;
	}

	public <TEntity> List<TEntity> read(String query, Object[] params, List<TEntity> HibernateRepository) {
		// TODO Auto-generated method stub
		return null;
	}

	public EnumerationValue withId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<EnumerationValue> allOnPage(PagingInfo page) {
		// TODO Auto-generated method stub
		return null;
	}

	public void add(EnumerationValue entity) {
		// TODO Auto-generated method stub
		
	}

	public void delete(EnumerationValue entity) {
		// TODO Auto-generated method stub
		
	}

	public void modify(EnumerationValue entity) {
		// TODO Auto-generated method stub
		
	}
	
}
