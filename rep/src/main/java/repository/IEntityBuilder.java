package repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import hibernate.EntityClass;

public interface IEntityBuilder<TEntity extends EntityClass> {
	
	public TEntity build(ResultSet rs) throws SQLException;
	 @SuppressWarnings({ "hiding" })
		public <TEntity> List<TEntity> read(String query, Object[] params, List<TEntity> HibernateRepository);
}