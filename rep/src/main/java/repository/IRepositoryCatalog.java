package repository;

import java.util.List;

import hibernate.Person;
import hibernate.UserRoles;

public interface IRepositoryCatalog {

	public IEnumerationValueRepository enumerations();
	public IUserRepository users();
	public void saveChanges();
	public IUserRepository getUsers();
	public IRepository<Person> getPersons();
	public IRepository<UserRoles> getRoles();
	public void commit();
	
	

		public <TEntity> List<TEntity> read(String query, Object[] params, List<TEntity> HibernateRepository);
}
