package repository;


import java.sql.SQLException;
import java.util.List;

import hibernate.User;
import hibernate.UserRoles;
public interface IUserRepository extends IRepository<User> {

	public User withLogin(String login);
	public User withLoginAndPassword(String login, String password);
	public void setupPermissions(User user);
	
	public List<User> withRole(UserRoles role);
	public List<User> withRole(String roleName);
	public List<User> withRole(int roleId);
	
		public <TEntity> List<TEntity> read(String query, Object[] params, List<TEntity> HibernateRepository);

		void setUpUpdateQuery(User entity) throws SQLException;

		

}
