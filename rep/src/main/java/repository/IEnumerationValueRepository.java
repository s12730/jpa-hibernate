package repository;

import java.sql.SQLException;
import java.util.List;

import hibernate.EnumerationValue;

public interface IEnumerationValueRepository extends IRepository<EnumerationValue> {

	public EnumerationValue withName(String name);
	public EnumerationValue withIntKey(int key, String name);
	public EnumerationValue withStringKey(String key, String name);
	 
	 
	 
	@SuppressWarnings({ })
		public <TEntity> List<TEntity> read(String query, Object[] params, List<TEntity> HibernateRepository);

	void setUpInsertQuery(EnumerationValue entity) throws SQLException;

	String getTableName();

	void setUpUpdateQuery(EnumerationValue entity) throws SQLException;
	String getInsertQuery();
	String getUpdateQuery();
}
