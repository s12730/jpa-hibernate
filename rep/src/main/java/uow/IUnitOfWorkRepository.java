package uow;

import java.util.List;

import hibernate.EntityClass;

public interface IUnitOfWorkRepository<TEntity> {

	public void persistAdd(EntityClass entity);
	public void persistDelete(EntityClass entity);
	public void persistUpdate(EntityClass entity);
	List<TEntity> read(String query, Object[] params);
	
}
