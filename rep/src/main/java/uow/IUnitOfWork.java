package uow;

import hibernate.EntityClass;

public interface IUnitOfWork {
	
	public void saveChanges();
	public void undo();
	@SuppressWarnings("rawtypes")
	public void markAsNew(EntityClass entity, IUnitOfWorkRepository repo);
	@SuppressWarnings("rawtypes")
	public void markAsDeleted(EntityClass entity, IUnitOfWorkRepository repo);
	@SuppressWarnings("rawtypes")
	public void markAsChanged(EntityClass entity,IUnitOfWorkRepository repo);

}
