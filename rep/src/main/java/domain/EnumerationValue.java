package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

@Entity
@Table(name = "t_p_enumeration_value")
@MappedSuperclass
public class EnumerationValue extends EntityClass {

	@Column(name = "int_key")
	private int intKey;
	@Column(name = "string_key")
	private String stringKey;
	@Column(name = "value")
	private String value;
	@Column(name = "enumeration_name")
	private String enumerationName;
	
	public int getIntKey() {
		return intKey;
	}
	public void setIntKey(int intKey) {
		this.intKey = intKey;
	}
	public String getStringKey() {
		return stringKey;
	}
	public void setStringKey(String stringKey) {
		this.stringKey = stringKey;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getEnumerationName() {
		return enumerationName;
	}
	public void setEnumerationName(String enumerationName) {
		this.enumerationName = enumerationName;
	}
	
	
	
}
