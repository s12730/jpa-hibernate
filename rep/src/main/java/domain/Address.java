package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

@Entity
@MappedSuperclass
@Table(name = "t_p_address")
public class Address extends EntityClass {

@Column(name = "country_id")
private int countryId ;
@Column(name = "region_id")
private int regionId ;
@Column(name = "city")
private String city ;
@Column(name = "street")
private String street ;
@Column(name = "house_number")
private String houseNumber ;
@Column(name = "local_number")
private String localNumber ;
@Column(name = "zip_code")
private String zipCode;
@Column(name = "type_id")
private int typeId ;
@ManyToOne
private Person person;

public Person getPerson() {
	return person;
}

public void setPerson(Person person) {
	this.person = person;
}

public Address() {}

public int getCountryId() {
	return countryId;
}

public void setCountryId(int countryId) {
	this.countryId = countryId;
}

public int getRegionId() {
	return regionId;
}

public void setRegionId(int regionId) {
	this.regionId = regionId;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

public String getStreet() {
	return street;
}

public void setStreet(String street) {
	this.street = street;
}

public String getHouseNumber() {
	return houseNumber;
}

public void setHouseNumber(String houseNumber) {
	this.houseNumber = houseNumber;
}

public String getLocalNumber() {
	return localNumber;
}

public void setLocalNumber(String localNumber) {
	this.localNumber = localNumber;
}

public String getZipCode() {
	return zipCode;
}

public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
}

public int getTypeId() {
	return typeId;
}

public void setTypeId(int typeId) {
	this.typeId = typeId;
}

 
}