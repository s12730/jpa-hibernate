package cache;

import hibernate.*;

public class Cache {

	private static Cache instance;

	public static Cache getInstance() {
		if (instance == null) {
			instance = new Cache();
		}
		return instance;
	}

	
	
	private BCache<Person> persons;
	private BCache<User> user;
	private BCache<Address> address;
	private BCache<PhoneNumber> phoneNumber;
	private BCache<EnumerationValue> enumerationValue;

	private Cache() {
		persons = new BCache<Person>(Person.class);
		user = new BCache<User>(User.class);
		address = new BCache<Address>(Address.class);
		phoneNumber = new BCache<PhoneNumber>(PhoneNumber.class);
		enumerationValue = new BCache<EnumerationValue>(EnumerationValue.class);
	}

	public BCache<Person> getPersons() {
		return persons;
	}

	public BCache<User> getUser() {
		return user;
	}

	public BCache<Address> getAddress() {
		return address;
	}

	public BCache<PhoneNumber> getPhoneNumber() {
		return phoneNumber;
	}

	public BCache<EnumerationValue> getEnumerationValue() {
		return enumerationValue;
	}

}