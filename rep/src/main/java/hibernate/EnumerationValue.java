package hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Table;


@Table(name = "t_p_enumeration_value")

public class EnumerationValue extends hibernate.EntityClass implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "int_key")
	private int intKey;
	@Column(name = "string_key")
	private String stringKey;
	@Column(name = "value")
	private String value;
	@Column(name = "enumeration_name")
	private String enumerationName;
	
	public int getIntKey() {
		return intKey;
	}
	public void setIntKey(int intKey) {
		this.intKey = intKey;
	}
	public String getStringKey() {
		return stringKey;
	}
	public void setStringKey(String stringKey) {
		this.stringKey = stringKey;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getEnumerationName() {
		return enumerationName;
	}
	public void setEnumerationName(String enumerationName) {
		this.enumerationName = enumerationName;
	}
	
	
	
}
