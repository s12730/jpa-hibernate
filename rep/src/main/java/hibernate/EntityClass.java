package hibernate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public abstract class EntityClass {

	private int id;
	private EntityState state;
	
	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Enumerated(EnumType.STRING)
	public EntityState getState() {
		return state;
	}
	public void setState(EntityState state) {
		this.state = state;
	}	
	
}
