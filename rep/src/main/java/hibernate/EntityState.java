package hibernate;

public enum EntityState {

	NEW, MODIFIED, UNCHANGED, DELETED, UNKNOWN; 
	
}
