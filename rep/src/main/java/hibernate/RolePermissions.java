package hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

@Entity
@Table(name = "role_permissions")
@MappedSuperclass

public class RolePermissions extends EntityClass {

	@Column(name = "role_id")
	private int roleId;
	@Column(name = "permission_id")
	private int permissionId;
	
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}
	
		
}
