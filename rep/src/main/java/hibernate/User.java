package hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Embeddable
@Table(name = "t_p_user")
@MappedSuperclass
public class User extends EntityClass {
	
	@Column(name = "login")
	private String login;
	@Column(name = "password")
	private String password;
	@OneToMany(mappedBy="UserRoles")
	@Enumerated(EnumType.STRING)
	private List<UserRoles> userRoles;
	@ManyToMany(mappedBy="RolePermissions")
	@Enumerated(EnumType.STRING)
	private List<RolePermissions> userPermissions;
	
	public User() {
		userRoles = new ArrayList<UserRoles>();
		userPermissions = new ArrayList<RolePermissions>();
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<UserRoles> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRoles> userRoles) {
		this.userRoles = userRoles;
	}

	public List<RolePermissions> getUserPermissions() {
		return userPermissions;
	}

	public void setUserPermissions(List<RolePermissions> userPermissions) {
		this.userPermissions = userPermissions;
	}
	
	public void addUserPermission(RolePermissions userPermission) {
		this.userPermissions.add(userPermission);
	}
	
	public void removeUserPermission(RolePermissions userPermission) {
		this.userPermissions.remove(userPermission);
	}
	
	public void addUserRole(UserRoles userRole) {
		this.userRoles.add(userRole);
	}
	
	public void RemoveUserRole(UserRoles userRole) {
		this.userRoles.remove(userRole);
	}

	public User(String login, String password, List<UserRoles> userRoles, List<RolePermissions> userPermissions) {
		super();
		this.login = login;
		this.password = password;
		this.userRoles = userRoles;
		this.userPermissions = userPermissions;
	}
	
	
	
	

}
