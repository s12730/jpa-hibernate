package hibernate;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;


@Table(name = "t_p_phone")
@MappedSuperclass
public class PhoneNumber extends hibernate.EntityClass implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Column(name = "country_prefix")
	private String countryPrefix;
	@Column(name = "city_prefix")
	private String cityPrefix;
	@Column(name = "number")
	private String numer;
	@Column(name = "type_id")
	private int typeId;

	private Person person;
	
	public String getCountryPrefix() {
		return countryPrefix;
	}
	public void setCountryPrefix(String countryPrefix) {
		this.countryPrefix = countryPrefix;
	}
	public String getCityPrefix() {
		return cityPrefix;
	}
	public void setCityPrefix(String cityPrefix) {
		this.cityPrefix = cityPrefix;
	}
	public String getNumer() {
		return numer;
	}
	public void setNumer(String numer) {
		this.numer = numer;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	
	
	
	
}
