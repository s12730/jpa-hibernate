package hibernate;

public enum Permission {
	
	ADD(0), DELETE(1), MODIFY(2);
	
	private int id;
	
	Permission(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

}
