package hibernate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class App {
	
	static EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPAService");
	static EntityManager em = emf.createEntityManager();

	public static void main(String[] args) {
		
		User user = new User("login", "password", null, null);
		em.persist(user);
		em.getTransaction().commit();
		em.close();
	}

}


